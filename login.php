<?php
header('Content-Type: text/html; charset=UTF-8');
session_start();

if (!empty($_SESSION['login'])) {
  header('Location: ./');
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>
<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="style.css" type="text/css" rel="stylesheet">
  <title>WB5</title>
</head>
<body>
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="">
        <form method="POST" class="">
          <div class="form-group">
            <label for="login-input">Логин</label>
            <input type="text" class="form-control" id="login-input" name="login" placeholder="Ваш логин">
          </div>
          <div class="form-group">
            <label for="pass-input">Пароль</label>
            <input type="password" class="form-control" id="pass-input" name="pass" placeholder="Ваш пароль">
          </div>
          <button type="submit" class="btn btn-primary">Войти</button>
        </form>
      </div>
    </div>
  </div>
</body>
</html>
<?php
}
else {

  $login = strip_tags($_POST['login']);
  $pass =  hash('md5', strip_tags($_POST['pass']), false);

  $db_user = 'u16432';   // Логин БД
  $db_pass = '2294342';  // Пароль БД

  $db = new PDO('mysql:host=localhost;dbname=u16432', $db_user, $db_pass, array(PDO::ATTR_PERSISTENT => true));

  try {

    $stmt = $db->prepare("SELECT * FROM wb5 WHERE login = ?");
    $stmt->execute(array($login));
    $user_data = $stmt->fetch();

    if ($pass == $user_data['pass']) {
      $_SESSION['login'] = $login;
    }
    else {
      echo "Неверные данные. Повторите попытку.";
      exit();
    }

  }
  catch(PDOException $e) {
    echo 'Ошибка: ' . $e->getMessage();
    exit();
  }

  header('Location: ./');
}
